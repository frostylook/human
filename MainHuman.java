package equals;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
public class MainHuman {
    static List<Human> humanList = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int input;
        do {
            printInstruction();
            input = scanner.nextInt();
            switch (input) {
                case 1:
                    addNewPerson(scanner);
                    break;
                case 2:
                    removePerson();
                    break;
                case 3:
                    checkIfEquals();
                    break;
                case 4:
                    printListOfPeople(humanList);
                    break;
            }
        } while (input != 5);
    }

    public static void printInstruction() {
        System.out.println("Choose:");
        System.out.println("1 - to add new person ");
        System.out.println("2 - to remove person from list");
        System.out.println("3 - to check if they are equals");
        System.out.println("4 - to print a list or");
        System.out.println("5 - to exit");
    }
    public static void addNewPerson(Scanner scanner) {
        System.out.println("Write a name:");
        String name = scanner.next();
        System.out.println("Write a surname:");
        String surname = scanner.next();
        System.out.println("Write an age:");
        int age = 0;
        boolean isCorrect = false;
        do {
            try {
                age = scanner.nextInt();
                isCorrect = true;
            } catch (InputMismatchException e) {
                System.out.println("Wrong input, text a number correctly");
                scanner.next();
            }
        } while (!isCorrect);

        Human human1 = new Human(name, surname, age);
        System.out.println(human1);
        humanList.add(human1);
    }

    public static void removePerson() {
        System.out.println("Do you want to remove person from a list? [yes or no]");
        String answer = scanner.next();
        if (answer.equals("yes")) {
            System.out.println("Write which person would you like to remove:");
            int personIndex = scanner.nextInt();
            humanList.remove(personIndex);
        } else if (answer.equals("no")) {
            System.out.println("Thank you for this answer.");
        }
    }
    public static void checkIfEquals() {
        System.out.println("People on which index would you like to compare?");
        System.out.println("Write first person:");
        int firstPerson = scanner.nextInt();
        Human human1 = humanList.get(firstPerson);
        System.out.println("Write second person");
        int secondPerson = scanner.nextInt();
        Human human2 = humanList.get(secondPerson);

        if (human1.equals(human2)) {
            System.out.println("People are the same.");
        } else {
            System.out.println("People are different.");
        }
    }
    public static void printListOfPeople(List<Human> humanList) {
        for (Human x : humanList)
            System.out.println("Place: " + humanList.indexOf(x) + " " + x);
    }
}